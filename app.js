const SwaggerRestify = require('swagger-restify-mw')
const restify = require('restify')
const jwt = require('restify-jwt')

const config = require('./config')

const app = restify.createServer()

module.exports = app; // for testing

const swaggerConfig = {
  appRoot: __dirname // required config
}

SwaggerRestify.create(swaggerConfig , function(err, swaggerRestify) {
  if (err) { throw err }

  app.use(jwt({
    secret: config.secret
  }).unless({
    path: [
      '/hello',
      '/swagger',
      '/auth'
    ],
    method: 'OPTIONS'
  }))

  swaggerRestify.register(app)

  const port = process.env.PORT || 10010
  app.listen(port)

  if (swaggerRestify.runner.swagger.paths['/hello']) {
    console.log('try this:\ncurl http://127.0.0.1:' + port + '/hello?name=Scott')
  }
})
