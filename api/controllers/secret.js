const jwt = require('jsonwebtoken')

const config = require('../../config')

module.exports = {
  mySecret
}

function mySecret(req, res) {
  const bearer = req.headers.authorization.split(' ')
  const decodedData = jwt.verify(bearer[1], config.secret)
  res.json(decodedData.username + ' - ' + decodedData.extra)
}
