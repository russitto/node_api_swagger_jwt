const jwt = require('jsonwebtoken')

const config = require('../../config')

module.exports = {
  login
}

function login(req, res) {
  const dataToEncode = {
    'username': 'MatiasR',
    'moreInfo': Date.now(),
    'extra': req.body.extra
  }
  const token = jwt.sign(dataToEncode, config.secret)
  res.json(token)
}
