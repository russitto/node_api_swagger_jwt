.. -*- coding: utf-8 -*-

===================================
Node.js Rest API with JWT + Swagger
===================================

.. image:: logo.png
  :scale: 200%

.. |date| date:: %d/%m/%Y %H:%M
.. |version| date:: %Y%m%d
.. contents:: Contenidos

.. header::

  ###Title### - Matias Russitto - matias@russitto.com

.. footer::

  ###Page### / ###Total###

.. raw:: pdf

  PageBreak

------------
Requeriments
------------

* Node.js + npm. Optional nvm: node version management
* require & module.exports knowledge
* swagger knowledge

----------------
What is swagger?
----------------

* Document that offers an API definition
* How to use it?

  - for compromise

  - by library or middleware

--------------------
Starting the project
--------------------

.. code-block:: bash

  $ npm install --global swagger 
  $ swagger project create -f restify _myapi_
  $ cd _myapi_
  $ node app

* Done! We have it a node.js API Rest working

--------------------
JWT: JSON Web Tokens
--------------------

.. code-block:: bash

  $ npm i -S restify-jwt

.. code-block:: javascript

  // app.js
  app.use(jwt({
    secret: 'shhhhhhared-secret'
  }).unless({
    path: ['/hello']
  }))

.. code-block:: yaml

  # swagger.yaml
  securityDefinitions:
    Bearer:
      description: authorization bearer
      type: apiKey
      name: Authorization
      in: header

  # in endpoints:
      security:
        - Bearer: []


.. raw:: pdf

  PageBreak

.. code-block:: javascript

  // myModule.js
  // encrypt / decrypt jwt
  const jwt = require('jsonwebtoken')

  const myObject = {
    sandia: "con vino",
    peppa: "pig"
  }
  const encodedData = jwt.sign(myObject, conf.jwt.secret)

  const decodedData = jwt.verify(token, secret)

----
Data
----

* Other APIs: *request*
* SQL Data Bases: *Sequelize*
* MongoDB: *Mongoose*
* Redis: *redis*
* …

----
Docs
----

* Taking advantage of the swagger file, using bootprint
* Doc CSS style can be modified using less files

.. code-block:: bash

  $ npm install --global bootprint bootprint-swagger
  $ bootprint api/swagger/swagger.yaml doc/

-----
Utils
-----

* StopLigth
* pm2
* jq

-----------------
Other framerworks
-----------------

* Next.js
* Nuxt.js
* Sails.js
