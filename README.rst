.. -*- coding: utf-8 -*-

===================================
Node.js Rest API with JWT + Swagger
===================================

.. image:: logo.png
  :scale: 200%

.. |date| date:: %d/%m/%Y %H:%M
.. |version| date:: %Y%m%d
.. contents:: Contenidos

.. header::

  ###Title### - Matias Russitto - matias@russitto.com

.. footer::

  ###Page### / ###Total###

.. raw:: pdf

  PageBreak

--------------
Requerimientos
--------------

* Node.js + npm, opcional nvm para manejo de versiones
* Conocimento de require y module.exports
* Conocimiento de swagger

----------------
¿Qué es swagger?
----------------

* Documento que brinda la definición de la API
* ¿Cómo usarlo?

  - Por compromiso

  - Mediante librería o middleware

---------------------
Empezando el proyecto
---------------------

.. code-block:: bash

  $ npm install --global swagger 
  $ swagger project create -f restify _myapi_
  $ cd _myapi_
  $ node app

* Con esto ya tenemos una Rest API funcionando en node.js

--------------------
JWT: JSON Web Tokens
--------------------

.. code-block:: bash

  $ npm i -S restify-jwt

.. code-block:: javascript

  // app.js
  app.use(jwt({
    secret: 'shhhhhhared-secret'
  }).unless({
    path: ['/hello']
  }))

.. code-block:: yaml

  # swagger.yaml
  securityDefinitions:
    Bearer:
      description: authorization bearer
      type: apiKey
      name: Authorization
      in: header

  # in endpoints:
      security:
        - Bearer: []


.. raw:: pdf

  PageBreak

.. code-block:: javascript

  // myModule.js
  // encrypt / decrypt jwt
  const jwt = require('jsonwebtoken')

  const myObject = {
    sandia: "con vino",
    peppa: "pig"
  }
  const encodedData = jwt.sign(myObject, conf.jwt.secret)

  const decodedData = jwt.verify(token, secret)

-----
Datos
-----

* Otras API: *request*
* Bases de datos SQL: *Sequelize*
* MongoDB: *Mongoose*
* Redis: *redis*
* …

-------------
Documentación
-------------

* Aprovechando el archivo swagger, mediante bootprint
* Se puede cambiar el estilo del CSS generado mediante la modificación de archivos de estilo less

.. code-block:: bash

  $ npm install --global bootprint bootprint-swagger
  $ bootprint api/swagger/swagger.yaml doc/

-----------
Utilitarios
-----------

* StopLigth
* pm2
* jq

-----------------
Otros framerworks
-----------------

* Next.js
* Nuxt.js
* Sails.js
